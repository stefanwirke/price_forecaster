import pytest
import pandas as pd

from price_forecaster.data.preprocessing import booleans
from price_forecaster.data.preprocessing import convert_value_format_dollar
from price_forecaster.data.preprocessing import convert_value_format_percentage
from price_forecaster.data.preprocessing import clean_zipcode
from price_forecaster.data.preprocessing import convert_date_to_int

# from price_forecaster.data.preprocessing import convert_amenities_to_cat


@pytest.fixture()
def boolean_raw_data():
    return pd.Series(["t", "f", "t", "t", "f"])


def test_booleans_are_converted(boolean_raw_data):
    result = booleans(boolean_raw_data)
    expected = pd.Series([True, False, True, True, False])
    assert result.dtype == expected.dtype
    pd.testing.assert_series_equal(result, expected)


@pytest.fixture()
def dollar_format_raw_data():
    return pd.Series(["$365.00", "$2,398.00", "$3,096.00", "$797.00", "$857.00"])


def test_conversion_of_dollar_format_to_numeric(dollar_format_raw_data):
    result = convert_value_format_dollar(dollar_format_raw_data)
    expected = pd.Series([365.0, 2398.0, 3096.0, 797.0, 857.0])
    pd.testing.assert_series_equal(result, expected)


@pytest.fixture()
def percentage_format_raw_data():
    return pd.Series(["150%", "350%", "850%", "600%", "300%"], dtype="string")


def test_conversion_of_percentage_to_int(percentage_format_raw_data):
    result = convert_value_format_percentage(percentage_format_raw_data)
    expected = pd.Series([150, 350, 850, 600, 300]).astype("Int32")
    pd.testing.assert_series_equal(result, expected)


@pytest.fixture()
def zip_code() -> pd.Series:
    return pd.Series(
        ["22000", "2000 frederiksberg", "2300.0", "test2100"], dtype="string"
    )


def test_zip_code(zip_code: pd.Series):
    result = clean_zipcode(zip_code)
    expected = pd.Series([2200, 2000, 2300, 2100]).astype("string")
    pd.testing.assert_series_equal(result, expected)


@pytest.fixture()
def time_as_host() -> pd.Series:
    return pd.Series(
        ["2020-10-12", "2020-10-17", "2020-11-09", "2020-09-21", "2020-10-19"]
    )


@pytest.mark.xfail
def test_convert_date_to_int(time_as_host: pd.Series):
    result = convert_date_to_int(time_as_host)
    expected = pd.Series([8, 3, -20, 29, 1])
    pd.testing.assert_series_equal(result, expected)


# @pytest.fixture()
# def convert_amenities_to_cat() -> pd.DataFrame:
#     return pd.DataFrame({})


def test_amenities_are_converted():
    # TODO Test number of columns when reading in two rows with different amenities
    pass


# '{TV,"Cable TV",Wifi,Kitchen,"Paid parking off premises",Heating,Washer,"Smoke alarm",
# Essentials,Hangers,
# "Hair dryer",Iron,"Laptop-friendly workspace","translation missing: en.hosting_amenity_49",
# "translation missing: en.hosting_amenity_50",
# "Room-darkening shades","Hot water","Bed linens",Microwave,"Coffee maker",Refrigerator,
# Dishwasher,"Dishes and silverware","Cooking basics",Oven,Stove,"Garden or backyard",
# "Luggage dropoff allowed","Paid parking on premises"}'

# '{TV,Wifi,Kitchen,"Indoor fireplace",Heating,"Family/kid friendly",Washer,
# Dryer,"Smoke alarm","Fire extinguisher",
# Essentials,Shampoo,Hangers,"Hair dryer",Iron,"Laptop-friendly workspace",
# Bathtub,"High chair",Crib,"Hot water",
# "Bed linens","Coffee maker",Refrigerator,"Cooking basics","Patio or balcony",
# "Garden or backyard","Long term stays allowed"}'
