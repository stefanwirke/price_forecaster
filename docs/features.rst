Features
========
| Feature discription of all our awesome features. No observations in dataset: 28523
| We want to predict the price we should set as a host, so we should focus on features that are in our control.
| Review related features are not in our control so should be excluded.
| Text-heavy features are left out for now, although there could be some interesting findings. Preproccessing will be very timely though.

id
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
| include: out
| datatype:  int64
| datatype to be:
| missing values %: None
| cardinality:  28523
| comments:


listing_url
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
| include: out
| datatype:  object
| datatype to be:
| missing values %: None
| cardinality:  28523
| comments:


scrape_id
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
| include: out
| datatype:  int64
| datatype to be:
| missing values %: None
| cardinality:  1
| comments:


last_scraped
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
| include: out
| datatype:  object
| datatype to be:
| missing values %: None
| cardinality:  5
| comments:


name
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
| include: out
| datatype:  object
| datatype to be: string
| missing values %:  0.2
| cardinality:  26923
| comments: "title" of listing. Interesting to see, which keywords are most popular. textmining.


summary
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
| include: out
| datatype:  object
| datatype to be: string
| missing values %:  3.84
| cardinality:  26988
| comments: Interesting to see which keywords, are most popular. text mining


space
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
| include: out
| datatype:  object
| datatype to be:
| missing values %:  39.93
| cardinality:  16807
| comments: Description of actual appartment. Interesting to see which keywords, are most popular. text mining.
| however, most of the text should be covered in other columns. eg. roomsize, etc.


description
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
| include: out
| datatype:  object
| datatype to be: string
| missing values %:  1.81
| cardinality:  27749
| comments: broad description of appartment and area. Intereseting to see which keywords are most popular. text mining.


experiences_offered
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
| include: out
| datatype:  object
| datatype to be:
| missing values %: None
| cardinality:  1
| comments:


neighborhood_overview
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
| include: out
| datatype:  object
| datatype to be: string
| missing values %:  43.5
| cardinality:  15521
| comments: description of area. Intereseting to see which keywords are most popular. text mining.


notes
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
| include: out
| datatype:  object
| datatype to be: string
| missing values %:  73.06
| cardinality:  7365
| comments: Intereseting to see which keywords are most popular. text mining.


transit
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
| include: out
| datatype:  object
| datatype to be: string
| missing values %:  39.67
| cardinality:  16720
| comments: description of transportation possibilities.
| Intereseting to see which keywords are most popular. text mining.


access
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
| include: out
| datatype:  object
| datatype to be: string
| missing values %:  55.22
| cardinality:  11258
| comments: amount of access. Whole appartment, room, etc.
| Intereseting to see which keywords are most popular. text mining.


interaction
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
| include: out
| datatype:  object
| datatype to be: string
| missing values %:  49.48
| cardinality:  13732
| comments: ways to interact with the host. Intereseting to see which keywords are most popular. text mining.


house_rules
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
| include: out
| datatype:  object
| datatype to be: string
| missing values %:  46.74
| cardinality:  13703
| comments: would need sentiment analysis if doing text mining?


thumbnail_url
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
| include: out
| datatype:  float64
| datatype to be:
| missing values %:  100.0
| cardinality:  0
| comments:


medium_url
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
| include: out
| datatype:  float64
| datatype to be:
| missing values %:  100.0
| cardinality:  0
| comments:


picture_url
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
| include: out
| datatype:  object
| datatype to be:
| missing values %: None
| cardinality:  28282
| comments:


xl_picture_url
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
| include: out
| datatype:  float64
| datatype to be:
| missing values %:  100.0
| cardinality:  0
| comments:


host_id
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
| include: out
| datatype:  int64
| datatype to be:
| missing values %: None
| cardinality:  25745
| comments:


host_url
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
| include: out
| datatype:  object
| datatype to be:
| missing values %: None
| cardinality:  25745
| comments:


host_name
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
| include: out
| datatype:  object
| datatype to be:
| missing values %:  0.04
| cardinality:  6415
| comments:


host_since
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
| include: out
| datatype:  object
| datatype to be: datetime
| missing values %:  0.04
| cardinality:  3379
| comments: might be interesting to see the relation between longevity and popularity of host


host_location
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
| include: out
| datatype:  object
| datatype to be:
| missing values %:  0.33
| cardinality:  865
| comments: looks like freetext and is very messy.
| should be redundant with zipcode and longitude, latitude


host_about
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
| include: out
| datatype:  object
| datatype to be: string
| missing values %:  49.12
| cardinality:  12498
| comments: Intereseting to see which keywords are most popular. text mining.


host_response_time
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
| include: keep
| datatype:  object
| datatype to be: cat
| missing values %:  80.28
| cardinality:  4
| comments:


host_response_rate
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
| include: keep
| datatype:  object
| datatype to be: int
| missing values %:  80.28
| cardinality:  43
| comments: would need to clean data for % signs. could be binned


host_acceptance_rate
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
| include: keep
| datatype:  object
| datatype to be: int
| missing values %:  43.64
| cardinality:  99
| comments: would need to clean data for % signs. could be binned


host_is_superhost
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
| include: keep
| datatype:  object
| datatype to be: boolean
| missing values %:  0.04
| cardinality:  2
| comments: values = f t, would need to be converted to True False


host_thumbnail_url
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
| include: out
| datatype:  object
| datatype to be:
| missing values %:  0.04
| cardinality:  25669
| comments:


host_picture_url
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
| include: out
| datatype:  object
| datatype to be:
| missing values %:  0.04
| cardinality:  25669
| comments:


host_neighbourhood
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
| include: keep
| datatype:  object
| datatype to be: cat
| missing values %:  28.07
| cardinality:  56
| comments: would probably need, "other" category. check for spelling.


host_listings_count
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
| include: out
| datatype:  float64
| datatype to be:
| missing values %:  0.04
| cardinality:  26
| comments: Not entirely sure what this is??


host_total_listings_count
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
| include: out
| datatype:  float64
| datatype to be:
| missing values %:  0.04
| cardinality:  26
| comments: Not entirely sure what this is??


host_verifications
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
| include: out
| datatype:  object
| datatype to be: cat / one-hot-encoding
| missing values %: None
| cardinality:  316
| comments: is a series of lists. Would need one-hot-encoding. but probably redundant, as we just need to know whether the host i verified or not


host_has_profile_pic
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
| include: keep
| datatype:  object
| datatype to be: boolean
| missing values %:  0.04
| cardinality:  2
| comments: values = f t, would need to be converted to True False


host_identity_verified
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
| include: keep
| datatype:  object
| datatype to be: boolean
| missing values %:  0.04
| cardinality:  2
| comments: values = f t, would need to be converted to True False


street
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
| include: out
| datatype:  object
| datatype to be:
| missing values %: None
| cardinality:  528
| comments: very messy. do NOT contain name of street. should be redundant with add. info


neighbourhood
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
| include: out
| datatype: object
| datatype to be: string
| missing values %: None
| cardinality:  21
| comments: not entirely sure of diff with host_neighbourhood. check for spelling differences and errors


neighbourhood_cleansed
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
| include: out
| datatype:  object
| datatype to be: string
| missing values %: None
| cardinality:  11
| comments: seem to be a shortend version of above, so maybe this is better?


neighbourhood_group_cleansed
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
| include: out
| datatype:  float64
| datatype to be:
| missing values %:  100.0
| cardinality:  0
| comments:


city
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
| include: out
| datatype:  object
| datatype to be:
| missing values %:  0.05
| cardinality:  158
| comments: we already have plenty of area specific data so should be redundant


state
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
| include: out
| datatype:  object
| datatype to be:
| missing values %:  84.22
| cardinality:  173
| comments: messy and redundant


zipcode
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
| include: keep
| datatype:  object
| datatype to be: int or string
| missing values %:  2.84
| cardinality:  737
| comments: contains text, so needs to be cleaned. regex or whatever
NB! dtype warning on this column

market
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
| include: out
| datatype:  object
| datatype to be:
| missing values %:  3.03
| cardinality:  10
| comments:


smart_location
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
| include: out
| datatype:  object
| datatype to be:
| missing values %: None
| cardinality:  158
| comments: redundant


country_code
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
| include: out
| datatype:  object
| datatype to be:
| missing values %: None
| cardinality:  1
| comments:


country
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
| include:out
| datatype:  object
| datatype to be:
| missing values %: None
| cardinality:  1
| comments:


latitude
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
| include: keep
| datatype:  float64
| datatype to be:
| missing values %: None
| cardinality:  7479
| comments:


longitude
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
| include: keep
| datatype:  float64
| datatype to be:
| missing values %: None
| cardinality:  11148
| comments:


is_location_exact
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
| include: out
| datatype:  object
| datatype to be:
| missing values %: None
| cardinality:  2
| comments:


property_type
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
| include: keep
| datatype:  object
| datatype to be: string / cat
| missing values %: None
| cardinality:  29
| comments: check for spelling. will probably need "Other" column


room_type
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
| include: keep
| datatype:  object
| datatype to be: cat
| missing values %: None
| cardinality:  4
| comments:


accommodates
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
| include: keep
| datatype:  int64
| datatype to be: int
| missing values %: None
| cardinality:  15
| comments: how many can stay over


bathrooms
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
| include: keep
| datatype:  float64
| datatype to be: float
| missing values %:  0.04
| cardinality:  14
| comments:


bedrooms
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
| include: keep
| datatype:  float64
| datatype to be: int
| missing values %:  0.1
| cardinality:  10
| comments:


beds
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
| include: keep
| datatype:  float64
| datatype to be: int
| missing values %:  0.36
| cardinality:  19
| comments: will need "Other" column


bed_type
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
| include: keep
| datatype:  object
| datatype to be: cat
| missing values %: None
| cardinality:  5
| comments:
| Hypothesis: All bed types different from real bed has a lower price


amenities
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
| include: keep
| datatype:  object
| datatype to be: string
| missing values %: None
| cardinality:  26634
| comments: series of hashes with options. will need one-hot-encoding


square_feet
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
| include: keep
| datatype:  float64
| datatype to be: int
| missing values %:  98.62
| cardinality:  121
| comments: could use some binning


price
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
| include: keep
| datatype:  object
| datatype to be: int
| missing values %: None
| cardinality:  611
| comments: needs to be converted from american format $1,000.00
TARGET!

weekly_price
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
| include: keep
| datatype:  object
| datatype to be: int
| missing values %:  87.68
| cardinality:  718
| comments: needs to be converted from american format $1,000.00 binning?
NB! dtype warning

monthly_price
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
| include: keep
| datatype:  object
| datatype to be: int
| missing values %:  94.56
| cardinality:  476
| comments: needs to be converted from american format $1,000.00 binning?
NB! dtype warning

security_deposit
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
| include: keep
| datatype:  object
| datatype to be: int
| missing values %:  48.54
| cardinality:  386
| comments: needs to be converted from american format $1,000.00 binning?


cleaning_fee
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
| include: keep
| datatype:  object
| datatype to be: int
| missing values %:  31.44
| cardinality:  430
| comments: needs to be converted from american format $1,000.00 binning?


guests_included
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
| include: keep
| datatype:  int64
| datatype to be: int
| missing values %: None
| cardinality:  14
| comments:


extra_people
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
| include: keep
| datatype:  object
| datatype to be: int
| missing values %: None
| cardinality:  265
| comments:needs to be converted from american format $1,000.00 binning?


minimum_nights
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
| include: keep
| datatype:  int64
| datatype to be: int
| missing values %: None
| cardinality:  70
| comments: will need "Other" column or binning


maximum_nights
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
| include: keep
| datatype:  int64
| datatype to be: int
| missing values %: None
| cardinality:  165
| comments: will need "Other" column or binning. max seems to be a default value


minimum_minimum_nights
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
| include: out
| datatype:  int64
| datatype to be:
| missing values %: None
| cardinality:  70
| comments: not sure what this is


maximum_minimum_nights
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
| include: out
| datatype:  int64
| datatype to be:
| missing values %: None
| cardinality:  77
| comments: not sure what this is


minimum_maximum_nights
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
| include: out
| datatype:  int64
| datatype to be:
| missing values %: None
| cardinality:  158
| comments: not sure what this is


maximum_maximum_nights
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
| include: out
| datatype:  int64
| datatype to be:
| missing values %: None
| cardinality:  158
| comments: not sure what this is


minimum_nights_avg_ntm
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
| include: out
| datatype:  float64
| datatype to be:
| missing values %: None
| cardinality:  190
| comments: not sure what this is


maximum_nights_avg_ntm
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
| include: out
| datatype:  float64
| datatype to be:
| missing values %: None
| cardinality:  225
| comments: not sure what this is


calendar_updated
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
| include: out
| datatype:  object
| datatype to be:
| missing values %: None
| cardinality:  82
| comments:


has_availability
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
| include: out
| datatype:  object
| datatype to be:
| missing values %: None
| cardinality:  1
| comments:


availability_30
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
| include: keep
| datatype:  int64
| datatype to be: int
| missing values %: None
| cardinality:  31
| comments: Probably how many days the listing were available in the last x days


availability_60
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
| include: keep
| datatype:  int64
| datatype to be: int
| missing values %: None
| cardinality:  61
| comments: Probably how many days the listing were available in the last x days


availability_90
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
| include: keep
| datatype:  int64
| datatype to be: int
| missing values %: None
| cardinality:  91
| comments: Probably how many days the listing were available in the last x days


availability_365
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
| include: keep
| datatype:  int64
| datatype to be: int
| missing values %: None
| cardinality:  366
| comments: Probably how many days the listing were available in the last x days


calendar_last_scraped
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
| include: out
| datatype:  object
| datatype to be:
| missing values %: None
| cardinality:  5
| comments:


number_of_reviews
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
| include: out
| datatype:  int64
| datatype to be: int / cat
| missing values %: None
| cardinality:  272
| comments: probably need some binning


number_of_reviews_ltm
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
| include: out
| datatype:  int64
| datatype to be: int / cat
| missing values %: None
| cardinality:  87
| comments: not sure what this is. binning


first_review
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
| include: out
| datatype:  object
| datatype to be:
| missing values %:  17.42
| cardinality:  2582
| comments:


last_review
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
| include: out
| datatype:  object
| datatype to be:
| missing values %:  17.42
| cardinality:  1846
| comments:


review_scores_rating
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
| include: out
| datatype:  float64
| datatype to be: int / cat
| missing values %:  19.1
| cardinality:  43
| comments: binning?


review_scores_accuracy
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
| include: out
| datatype:  float64
| datatype to be: int
| missing values %:  19.17
| cardinality:  9
| comments:


review_scores_cleanliness
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
| include: out
| datatype:  float64
| datatype to be: int
| missing values %:  19.16
| cardinality:  9
| comments:


review_scores_checkin
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
| include: out
| datatype:  float64
| datatype to be: int
| missing values %:  19.24
| cardinality:  8
| comments:


review_scores_communication
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
| include: out
| datatype:  float64
| datatype to be: int
| missing values %:  19.18
| cardinality:  8
| comments:


review_scores_location
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
| include: out
| datatype:  float64
| datatype to be: int
| missing values %:  19.25
| cardinality:  8
| comments:


review_scores_value
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
| include: out
| datatype:  float64
| datatype to be: int
| missing values %:  19.27
| cardinality:  8
| comments:


requires_license
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
| include: out
| datatype:  object
| datatype to be:
| missing values %: None
| cardinality:  1
| comments:


license
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
| include: out
| datatype:  object
| datatype to be:
| missing values %:  99.96
| cardinality:  2
| comments:
NB! dtype warning

jurisdiction_names
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
| include: out
| datatype:  float64
| datatype to be:
| missing values %:  100.0
| cardinality:  0
| comments:


instant_bookable
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
| include: keep
| datatype:  object
| datatype to be: boolean
| missing values %: None
| cardinality:  2
| comments: t + f will need to be converted to True False


is_business_travel_ready
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
| include: out
| datatype:  object
| datatype to be:
| missing values %: None
| cardinality:  1
| comments:


cancellation_policy
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
| include: keep
| datatype:  object
| datatype to be: cat
| missing values %: None
| cardinality:  4
| comments:


require_guest_profile_picture
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
| include: keep
| datatype:  object
| datatype to be: boolean
| missing values %: None
| cardinality:  2
| comments: t + f will need to be converted to True False


require_guest_phone_verification
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
| include: keep
| datatype:  object
| datatype to be: boolean
| missing values %: None
| cardinality:  2
| comments: t + f will need to be converted to True False


calculated_host_listings_count
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
| include: out
| datatype:  int64
| datatype to be:
| missing values %: None
| cardinality:  21
| comments:


calculated_host_listings_count_entire_homes
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
| include: out
| datatype:  int64
| datatype to be:
| missing values %: None
| cardinality:  20
| comments:


calculated_host_listings_count_private_rooms
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
| include: out
| datatype:  int64
| datatype to be:
| missing values %: None
| cardinality:  11
| comments:


calculated_host_listings_count_shared_rooms
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
| include: out
| datatype:  int64
| datatype to be:
| missing values %: None
| cardinality:  6
| comments:


reviews_per_month
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
| include: out
| datatype:  float64
| datatype to be: float
| missing values %:  17.42
| cardinality:  510
| comments: binning
