from ml_tooling.transformers import (
    Pipeline,
    DFFeatureUnion,
    Select,
    FillNA,
    ToCategorical,
)


categories = Pipeline(
    [
        (
            "select",
            Select(
                [
                    "host_response_time",
                    "bed_type",
                    "room_type",
                    "property_type",
                    "cancellation_policy",
                    "host_neighbourhood",
                ]
            ),
        ),
        ("fillna", FillNA("missing", indicate_nan=True)),
        ("categorical", ToCategorical()),
    ]
)


feature_pipeline = DFFeatureUnion([("categorical", categories)])
