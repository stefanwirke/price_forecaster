from ml_tooling import Model
from sklearn.ensemble import RandomForestRegressor
import xgboost as xgb
from price_forecaster.data.dataset import PriceForecasterDataset
from sklearn.dummy import DummyRegressor
from sklearn.linear_model import LinearRegression
from price_forecaster.config import RAW_DIR
from price_forecaster.features.build_features import feature_pipeline

pfd = PriceForecasterDataset(RAW_DIR / "data_2020-06-26.csv")


# model = Model(x, feature_pipeline=feature_pipeline)
def train_model():
    best_model, results = Model.test_estimators(
        pfd,
        [
            xgb.XGBRegressor(),
            DummyRegressor(strategy="median"),
            LinearRegression(),
            RandomForestRegressor(),
        ],
        metrics=["r2", "neg_mean_squared_error"],
        feature_pipeline=feature_pipeline,
    )
    return best_model, results


if __name__ == "__main__":
    train_model()
