all_columns = [
    "id",
    "listing_url",
    "scrape_id",
    "last_scraped",
    "name",
    "summary",
    "space",
    "description",
    "experiences_offered",
    "neighborhood_overview",
    "notes",
    "transit",
    "access",
    "interaction",
    "house_rules",
    "thumbnail_url",
    "medium_url",
    "picture_url",
    "xl_picture_url",
    "host_id",
    "host_url",
    "host_name",
    "host_since",
    "host_location",
    "host_about",
    "host_response_time",
    "host_response_rate",
    "host_acceptance_rate",
    "host_is_superhost",
    "host_thumbnail_url",
    "host_picture_url",
    "host_neighbourhood",
    "host_listings_count",
    "host_total_listings_count",
    "host_verifications",
    "host_has_profile_pic",
    "host_identity_verified",
    "street",
    "neighbourhood",
    "neighbourhood_cleansed",
    "neighbourhood_group_cleansed",
    "city",
    "state",
    "zipcode",
    "market",
    "smart_location",
    "country_code",
    "country",
    "latitude",
    "longitude",
    "is_location_exact",
    "property_type",
    "room_type",
    "accommodates",
    "bathrooms",
    "bedrooms",
    "beds",
    "bed_type",
    "amenities",
    "square_feet",
    "price",
    "weekly_price",
    "monthly_price",
    "security_deposit",
    "cleaning_fee",
    "guests_included",
    "extra_people",
    "minimum_nights",
    "maximum_nights",
    "minimum_minimum_nights",
    "maximum_minimum_nights",
    "minimum_maximum_nights",
    "maximum_maximum_nights",
    "minimum_nights_avg_ntm",
    "maximum_nights_avg_ntm",
    "calendar_updated",
    "has_availability",
    "availability_30",
    "availability_60",
    "availability_90",
    "availability_365",
    "calendar_last_scraped",
    "number_of_reviews",
    "number_of_reviews_ltm",
    "first_review",
    "last_review",
    "review_scores_rating",
    "review_scores_accuracy",
    "review_scores_cleanliness",
    "review_scores_checkin",
    "review_scores_communication",
    "review_scores_location",
    "review_scores_value",
    "requires_license",
    "license",
    "jurisdiction_names",
    "instant_bookable",
    "is_business_travel_ready",
    "cancellation_policy",
    "require_guest_profile_picture",
    "require_guest_phone_verification",
    "calculated_host_listings_count",
    "calculated_host_listings_count_entire_homes",
    "calculated_host_listings_count_private_rooms",
    "calculated_host_listings_count_shared_rooms",
    "reviews_per_month",
]

selected_columns = {
    "host_since": "string",
    "host_response_time": "category",
    "bed_type": "category",
    "room_type": "category",
    "property_type": "category",
    "cancellation_policy": "category",
    "host_response_rate": "string",
    "host_acceptance_rate": "string",
    "zipcode": "string",
    "amenities": "string",
    "host_neighbourhood": "category",
    "latitude": "float32",
    "longitude": "float32",
    "square_feet": "float32",
    "bathrooms": "float32",
    "bedrooms": "Int8",
    "beds": "Int8",
    "guests_included": "Int32",
    "accommodates": "Int8",
    "minimum_nights": "Int32",
    "maximum_nights": "Int32",
    "availability_30": "Int32",
    "availability_60": "Int32",
    "availability_90": "Int32",
    "availability_365": "Int32",
    "price": "string",
    "weekly_price": "string",
    "monthly_price": "string",
    "security_deposit": "string",
    "cleaning_fee": "string",
    "extra_people": "string",
    "instant_bookable": "string",
    "host_is_superhost": "string",
    "host_has_profile_pic": "string",
    "host_identity_verified": "string",
    "require_guest_phone_verification": "string",
    "require_guest_profile_picture": "string",
}
