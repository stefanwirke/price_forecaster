import tempfile
from urllib.request import urlretrieve
import gzip
from price_forecaster.config import RAW_DIR


def download_data(year: int, month: int, day: int):
    url = (
        f"http://data.insideairbnb.com/denmark/hovedstaden/copenhagen/"
        f"{year}-{month:02}-{day:02}/data/listings.csv.gz"
    )

    output_file = RAW_DIR / f"data_{year}-{month:02}-{day:02}.csv"

    # Setup a tempfile to download into - download as binary
    with tempfile.NamedTemporaryFile(mode="wb") as temp:
        urlretrieve(url, temp.name)
        # unzip the gzipped file
        with gzip.open(temp.name, "rt") as f:
            output_file.write_text(f.read())
    return output_file
