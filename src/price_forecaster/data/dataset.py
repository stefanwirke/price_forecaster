import pandas as pd
from ml_tooling.data.file import FileDataset
from price_forecaster.data.data_columns import selected_columns
from price_forecaster.config import RAW_DIR, PROCESSED_DIR
import tempfile
from urllib.request import urlretrieve
import gzip
from price_forecaster.data.preprocessing import preprocessing


class PriceForecasterDataset(FileDataset):
    """
    super skal implementeres
    """

    @classmethod
    def from_download(cls, year, month, day):
        """

        Parameters
        ----------
        year
        month
        day

        Returns
        -------

        """
        url = (
            f"http://data.insideairbnb.com/denmark/hovedstaden/copenhagen/"
            f"{year}-{month:02}-{day:02}/data/listings.csv.gz"
        )

        output_file = RAW_DIR / f"data_{year}-{month:02}-{day:02}.csv"

        # Setup a tempfile to download into - download as binary
        with tempfile.NamedTemporaryFile(mode="wb") as temp:
            urlretrieve(url, temp.name)
            # unzip the gzipped file
            with gzip.open(temp.name, "rt") as f:
                output_file.write_text(f.read())
                # TODO open file from parquet
        return cls(output_file)

    # noinspection PyTypeChecker
    def preprocessing_data(self) -> None:
        raw_data = self.read_file(
            usecols=list(selected_columns.keys()), dtype=selected_columns
        )
        processed_data = preprocessing(raw_data)
        processed_data.to_parquet(PROCESSED_DIR / "data.parquet")
        return None

    def load_processed_data(self) -> pd.DataFrame:
        self.preprocessing_data()
        return pd.read_parquet(PROCESSED_DIR / "data.parquet")

    def load_training_data(self):
        data = self.load_processed_data()
        return data.drop(columns="price"), data.price

    def load_prediction_data(self, idx):
        data = self.load_processed_data()
        return data.drop(columns=["price"]).iloc[[idx]]
