import pandas as pd
from datetime import datetime


def booleans(series: pd.Series) -> pd.Series:
    return series.replace({"t": True, "f": False}).fillna(False).astype("bool")

    # host_is_superhost
    # host_has_profile_pic
    # instant_bookable
    # require_guest_profile_picture
    # require_guest_phone_verification


def convert_value_format_dollar(series: pd.Series) -> pd.Series:
    new_series = series.str.replace("$", "")
    new_series = new_series.str.replace(",", "")
    return pd.to_numeric(new_series, errors="coerce")

    # 'price': "string",
    # 'weekly_price': "string",
    # 'monthly_price': "string",
    # 'security_deposit': "string",
    # 'cleaning_fee': "string",
    # 'extra_people': "string",


def convert_value_format_percentage(series: pd.Series) -> pd.Series:
    return series.str.replace("%", "").astype("Int32")


def clean_zipcode(series: pd.Series) -> pd.Series:
    new_series = series.str.extract(r"(\d{4})", expand=False)
    return new_series


def convert_date_to_int(series: pd.Series) -> pd.Series:
    new_series = datetime.now() - pd.to_datetime(series)
    return new_series.apply(lambda x: x.days)


def convert_amenities_to_cat(series: pd.Series) -> pd.DataFrame:
    return (
        series.str.replace('"', "")
        .str.replace("{", "")
        .str.replace("}", "")
        .str.split(",", expand=False)
        .apply(lambda x: pd.Series(dict.fromkeys(x, 1)))
        .fillna(0)
        .astype("bool")
    )


def preprocessing(df: pd.DataFrame) -> pd.DataFrame:
    df.host_is_superhost = booleans(df.host_is_superhost)
    df.host_has_profile_pic = booleans(df.host_has_profile_pic)
    df.host_identity_verified = booleans(df.host_identity_verified)
    df.instant_bookable = booleans(df.instant_bookable)
    df.require_guest_profile_picture = booleans(df.require_guest_profile_picture)
    df.require_guest_phone_verification = booleans(df.require_guest_phone_verification)

    df.price = convert_value_format_dollar(df.price)
    df.weekly_price = convert_value_format_dollar(df.weekly_price)
    df.monthly_price = convert_value_format_dollar(df.monthly_price)
    df.security_deposit = convert_value_format_dollar(df.security_deposit)

    df.cleaning_fee = convert_value_format_dollar(df.cleaning_fee)
    df.extra_people = convert_value_format_dollar(df.extra_people)
    df.host_response_rate = convert_value_format_percentage(df.host_response_rate)
    df.host_acceptance_rate = convert_value_format_percentage(df.host_acceptance_rate)

    df.zipcode = clean_zipcode(df.zipcode)
    df.zipcode = df.zipcode.astype("category")

    df.host_since = convert_date_to_int(df.host_since)

    df = pd.concat([df, convert_amenities_to_cat(df.amenities)], axis=1)
    return df
