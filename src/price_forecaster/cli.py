import click
from price_forecaster.data.dataset import PriceForecasterDataset
from ml_tooling import Model
from price_forecaster.features.build_features import feature_pipeline
from sklearn.dummy import DummyRegressor
from price_forecaster.config import RAW_DIR

model = Model(DummyRegressor(), feature_pipeline=feature_pipeline)

pfd = PriceForecasterDataset(RAW_DIR / "data_2020-06-26.csv")


@click.group()
def cli():
    pass


@cli.command()
@click.option("--cv", default=10, type=int, help="Number of cross-validations to do")
def train(cv: int):
    click.secho(f"Training with {cv} splits...", fg="green")
    result = model.score_estimator(pfd, cv=cv)
    click.echo(result)


@cli.command()
def predict():
    click.secho("predicting")


if __name__ == "__main__":
    train()
